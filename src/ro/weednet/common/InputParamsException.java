package ro.weednet.common;

public class InputParamsException extends JSONException {
	private static final long serialVersionUID = -4046510049016713362L;
	
	public InputParamsException() {
		super(400, "invalid input params");
	}
}