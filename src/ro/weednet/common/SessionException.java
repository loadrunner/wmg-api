package ro.weednet.common;

public class SessionException extends JSONException {
	private static final long serialVersionUID = -4046510049016713362L;
	
	public SessionException() {
		super(400, "invalid session id");
	}
}