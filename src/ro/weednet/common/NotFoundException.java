package ro.weednet.common;

public class NotFoundException extends JSONException {
	private static final long serialVersionUID = -4046510049016713362L;
	
	public NotFoundException() {
		super(404, "Nof Found");
	}
}