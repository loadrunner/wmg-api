package ro.weednet.common;

import com.google.code.morphia.Morphia;
import com.google.code.morphia.dao.BasicDAO;
import com.mongodb.Mongo;

public class BaseDAO<T, K> extends BasicDAO<T, K> {
	protected BaseDAO(Mongo mongo, Morphia morphia, String dbName) {
		super(mongo, morphia, dbName);
	}
	
	public T findOne(Object value) {
		return findOne("_id", value);
	}
}