package ro.weednet.common;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.BasicBSONObject;


public class JSONException extends WebApplicationException {
	private static final long serialVersionUID = -4046510049016713362L;
	
	public JSONException(int code) {
		super(Response.status(code).entity(constructMessage(code, "")).type(MediaType.APPLICATION_JSON).build());
	}
	public JSONException(int code, String message) {
		super(Response.status(code).entity(constructMessage(code, message)).type(MediaType.APPLICATION_JSON).build());
	}
	public JSONException(BasicBSONObject r) {
		super(Response.status(r.getInt("code")).entity(r.toString()).type(MediaType.APPLICATION_JSON).build());
	}
	
	private static String constructMessage(int code, String message) {
		BasicBSONObject r = new BasicBSONObject();
		r.put("code", code);
		r.put("message", message);
		
		return r.toString();
	}
}