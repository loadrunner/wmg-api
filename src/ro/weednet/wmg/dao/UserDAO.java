package ro.weednet.wmg.dao;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import ro.weednet.common.BaseDAO;
import ro.weednet.wmg.Server;
import ro.weednet.wmg.model.User;

import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class UserDAO extends BaseDAO<User, ObjectId> {
	protected UserDAO(Mongo mongo, Morphia morphia, String dbName) {
		super(mongo, morphia, dbName);
	}

	private static UserDAO _instance = null;
	
	public static UserDAO singleton() throws UnknownHostException, MongoException {
		if (_instance == null) {
			
			Morphia morphia = new Morphia();
			_instance = new UserDAO(Server.singleton().getMongo(), morphia, Server.singleton().getConfig("mongo.db"));
		}
		
		return _instance;
	}
}
