package ro.weednet.wmg.dao;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import ro.weednet.wmg.Server;
import ro.weednet.common.BaseDAO;
import ro.weednet.wmg.model.Currency;

import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class CurrencyDAO extends BaseDAO<Currency, ObjectId> {
	protected CurrencyDAO(Mongo mongo, Morphia morphia, String dbName) {
		super(mongo, morphia, dbName);
	}
	
	private static CurrencyDAO _instance = null;
	
	public static CurrencyDAO singleton() throws UnknownHostException, MongoException {
		if (_instance == null) {
			
			Morphia morphia = new Morphia();
			_instance = new CurrencyDAO(Server.singleton().getMongo(), morphia, Server.singleton().getConfig("mongo.db"));
		}
		
		return _instance;
	}
}
