package ro.weednet.wmg.dao;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import ro.weednet.common.BaseDAO;
import ro.weednet.wmg.Server;
import ro.weednet.wmg.model.Transaction;

import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class TransactionDAO extends BaseDAO<Transaction, ObjectId> {
	protected TransactionDAO(Mongo mongo, Morphia morphia, String dbName) {
		super(mongo, morphia, dbName);
	}

	private static TransactionDAO _instance = null;
	
	public static TransactionDAO singleton() throws UnknownHostException, MongoException {
		if (_instance == null) {
			
			Morphia morphia = new Morphia();
			_instance = new TransactionDAO(Server.singleton().getMongo(), morphia, Server.singleton().getConfig("mongo.db"));
		}
		
		return _instance;
	}
}
