package ro.weednet.wmg.dao;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import ro.weednet.common.BaseDAO;
import ro.weednet.wmg.Server;
import ro.weednet.wmg.model.Session;

import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class SessionDAO extends BaseDAO<Session, ObjectId> {
	protected SessionDAO(Mongo mongo, Morphia morphia, String dbName) {
		super(mongo, morphia, dbName);
	}

	private static SessionDAO _instance = null;
	
	public static SessionDAO singleton() throws UnknownHostException, MongoException {
		if (_instance == null) {
			
			Morphia morphia = new Morphia();
			_instance = new SessionDAO(Server.singleton().getMongo(), morphia, Server.singleton().getConfig("mongo.db"));
		}
		
		return _instance;
	}
}
