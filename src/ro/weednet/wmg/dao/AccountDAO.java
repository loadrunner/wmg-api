package ro.weednet.wmg.dao;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import ro.weednet.common.BaseDAO;
import ro.weednet.wmg.Server;
import ro.weednet.wmg.model.Account;

import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class AccountDAO extends BaseDAO<Account, ObjectId> {
	protected AccountDAO(Mongo mongo, Morphia morphia, String dbName) {
		super(mongo, morphia, dbName);
	}
	
	private static AccountDAO _instance = null;
	
	public static AccountDAO singleton() throws UnknownHostException, MongoException {
		if (_instance == null) {
			
			Morphia morphia = new Morphia();
			_instance = new AccountDAO(Server.singleton().getMongo(), morphia, Server.singleton().getConfig("mongo.db"));
		}
		
		return _instance;
	}
}
