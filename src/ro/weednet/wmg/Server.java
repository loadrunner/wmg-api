package ro.weednet.wmg;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.mongodb.DB;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class Server extends Thread {
	public final static String namespace = "ro.weednet.wmg";
	private static Server _instance = null;
	private static boolean _init_run = false;
	private static Properties _config = new Properties();
	private static Logger _log;
	private static Mongo _mongo = null;
	private static DB _db = null;
	private AtomicBoolean _active = new AtomicBoolean(true);
	private AtomicBoolean _paused = new AtomicBoolean(false);
	
	public Server() {
		if (!_init_run) {
			return;
		}
		
		setDaemon(true);
	}
	
	public static Server singleton() {
		if (_instance == null && _init_run) {
			_instance = new Server();
			_instance.start();
		}
		
		return _instance;
	}
	public void run() {
		while (_active.get()) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				_log.warning(e.toString());
				break;
			}
		}
		
		_log.info("closing main thread");
	}
	
	public synchronized void pause() {
		try {
			_paused.set(true);
			_log.finer("pausing  ..");
			wait();
		} catch (InterruptedException e) {
			_log.warning("pause failed (" + e.toString() + ")");
		//	_active.set(false);
		}
	}
	public synchronized void force() {
		if (_paused.get()) {
			_paused.set(false);
			_log.info("forced");
			notify();
		}
	}
	
	public Mongo getMongo() {
		return _mongo;
	}
	public DB getDb() {
		return _db;
	}
	public String getConfig(String key) {
		return getConfig(key, false);
	}
	public String getConfig(String key, boolean absolute) {
		if(!absolute) {
			key = namespace + "." + key;
		}
		return _config.getProperty(key);
	}
	
	public static void init() {
		if (_init()) {
			_init_run = true;
		}
	}
	protected static boolean _init() {
		System.setProperty("java.util.logging.config.file", System.getProperty("catalina.home") + "/conf/logging.properties");
		try {
			LogManager.getLogManager().readConfiguration();
		} catch (SecurityException | IOException e) {
			System.out.println("FATAL EROR: " + e.toString());
			return false;
		}
		_log = Logger.getLogger("ro.weednet.wmg");
		_log.setLevel(Level.FINEST);
		
		
	//	_log.severe(System.getProperty("user.dir"));
	//	_log.severe(System.getProperty("java.util.logging.config.file"));
		/*
		_log.severe("doing stucxff");
		_log.warning("doing stucxff");
		_log.info("doing stucxff");
		_log.config("doing stucxff");
		_log.fine("doing stucxff");
		_log.finer("doing stucxff");
		_log.finest("doing stucxff");
		*/
		try {
			_config.load(new FileInputStream(new File(System.getProperty("catalina.home") + "/conf/general.properties")));
		} catch (IOException e) {
			_log.severe("config file not found!" + e.toString());
			return false;
		}
		System.setProperty("ro.cauti.importer.temp_path", System.getProperty("catalina.home") + "/temp");
		
		String host = _config.getProperty("ro.weednet.wmg.mongo.host");
		int port;
		String db = _config.getProperty("ro.weednet.wmg.mongo.db");
		
		try {
			port = Integer.parseInt(_config.getProperty("ro.weednet.wmg.mongo.port"));
		} catch (Exception e) {
			_log.severe("invalid db port" + _config.getProperty("ro.weednet.wmg.mongo.port"));
			return false;
		}
		
		try {
			_mongo = new Mongo(host, port);
			_db = _mongo.getDB(db);
		} catch (UnknownHostException | MongoException e) {
			_log.severe("cannot connect to mongoDB (" + e.toString() + ")");
			return false;
		}
		
		return true;
	}
}
