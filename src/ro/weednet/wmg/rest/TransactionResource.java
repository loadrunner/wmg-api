package ro.weednet.wmg.rest;

import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import com.mongodb.MongoException;

import ro.weednet.common.InputParamsException;
import ro.weednet.wmg.dao.TransactionDAO;
import ro.weednet.wmg.model.Tag;
import ro.weednet.wmg.model.Transaction;

public class TransactionResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	String id;
	public TransactionResource(UriInfo uriInfo, Request request, String id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Transaction get() throws UnknownHostException, MongoException {
		Transaction transaction = TransactionDAO.singleton().findOne("_id", new ObjectId(id));
		
		if (transaction == null) {
			throw new InputParamsException();
		}
		
		return transaction;
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public String post(
		@FormParam("description") String description,
		@FormParam("value") double value,
		@FormParam("rate") double rate,
		@FormParam("total") double total,
		@FormParam("currency") String currency,
		@FormParam("timestamp") int timestamp
	) throws UnknownHostException, MongoException {
		
		TransactionDAO dao = TransactionDAO.singleton();
		Transaction transaction = dao.findOne("_id", new ObjectId(id));
		
		if (transaction == null) {
			throw new InputParamsException();
		}
		
		transaction.setTags(new ArrayList<Tag>());
		transaction.setDescription(description);
		transaction.setValue(value);
		transaction.setRate(rate);
		transaction.setTotal(total);
		transaction.setCurrency(currency);
		transaction.setTimestamp(timestamp);
		dao.save(transaction);
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response add(JAXBElement<Transaction> transaction) {
		Transaction t = transaction.getValue();
		return putAndGetResponse(t);
	}
	
	@DELETE
	public String delete() throws UnknownHostException, MongoException {
		TransactionDAO dao = TransactionDAO.singleton();
		Transaction transaction = dao.findOne("_id", new ObjectId(id));
		if (transaction == null) {
			throw new InputParamsException();
		}
		Transaction twin = transaction.getTwin();
		if (twin != null) {
			dao.delete(twin);
		}
		dao.delete(transaction);
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	private Response putAndGetResponse(Transaction transaction) {
		Response res = null;
	//	if(TransactionDAO.singleton().containsKey(transaction.getId())) {
	//		res = Response.noContent().build();
	//	} else {
	//		res = Response.created(uriInfo.getAbsolutePath()).build();
	//	}
	//	TodoDao.instance.getModel().put(transaction.getId(), transaction);
		return res;
	}
}
