package ro.weednet.wmg.rest;

import java.net.UnknownHostException;

import java.util.Calendar;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.bson.BSONObject;

import com.mongodb.MongoException;

import ro.weednet.common.InputParamsException;
import ro.weednet.wmg.model.Tag;
import ro.weednet.wmg.model.User;

public class TagResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	User user;
	String name;
	
	public TagResource(UriInfo uriInfo, Request request, User user, String name) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.user = user;
		this.name = name;
	}
	
	//Application integration
	@SuppressWarnings("deprecation")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public String get() throws UnknownHostException, MongoException {
		
		int interval = Calendar.DATE,
			period = 30;
		
		Calendar date = Calendar.getInstance();
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.add(interval, -period);
		
		BSONObject stats = Tag.getStats(user, name, date, interval, period);
		
		if (stats == null) {
			throw new InputParamsException();
		}
		
		return stats.toString();
	}
}
