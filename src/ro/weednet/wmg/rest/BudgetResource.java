package ro.weednet.wmg.rest;

import java.net.UnknownHostException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import org.bson.types.ObjectId;

import com.mongodb.MongoException;
import com.mongodb.WriteResult;

import ro.weednet.common.InputParamsException;
import ro.weednet.wmg.dao.BudgetDAO;
import ro.weednet.wmg.dao.TransactionDAO;
import ro.weednet.wmg.model.Budget;

public class BudgetResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	String id;
	public BudgetResource(UriInfo uriInfo, Request request, String id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}
	
	//Application integration
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Budget get() throws UnknownHostException, MongoException {
		Budget budget = BudgetDAO.singleton().findOne("_id", new ObjectId(id));
		
		if (budget == null) {
			throw new InputParamsException();
		}
		
		return budget;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response add(JAXBElement<Budget> budget) {
		Budget g = budget.getValue();
		return putAndGetResponse(g);
	}
	
	@DELETE
	public void delete() throws UnknownHostException, MongoException {
		TransactionDAO dao = TransactionDAO.singleton();
		if (dao.exists(dao.createQuery().filter("budget_id", new ObjectId(id)))) {
			Budget budget = BudgetDAO.singleton().findOne("_id", new ObjectId(id));
			
			if (budget == null) {
				throw new InputParamsException();
			}
			
			budget.setHidden(true);
			BudgetDAO.singleton().save(budget);
		} else {
			WriteResult r = BudgetDAO.singleton().deleteById(new ObjectId(id));
			if (r == null) {
				throw new InputParamsException();
			}
		}
	}
	
	private Response putAndGetResponse(Budget budget) {
		Response res = null;
	//	if(TransactionDAO.singleton().containsKey(transaction.getId())) {
	//		res = Response.noContent().build();
	//	} else {
	//		res = Response.created(uriInfo.getAbsolutePath()).build();
	//	}
	//	TodoDao.instance.getModel().put(transaction.getId(), transaction);
		return res;
	}
}