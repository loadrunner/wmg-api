package ro.weednet.wmg.rest;

import java.net.UnknownHostException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import com.mongodb.MongoException;
import com.mongodb.WriteResult;

import ro.weednet.common.InputParamsException;
import ro.weednet.common.JSONException;
import ro.weednet.wmg.dao.AccountDAO;
import ro.weednet.wmg.dao.TransactionDAO;
import ro.weednet.wmg.model.Account;

public class AccountResource {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	String id;
	public AccountResource(UriInfo uriInfo, Request request, String id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Account get() throws UnknownHostException, MongoException {
		Account account = AccountDAO.singleton().findOne("_id", new ObjectId(id));
		
		if (account == null) {
			throw new InputParamsException();
		}
		
		account.calculateBalance();
		
		return account;
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public String post(@FormParam("name") String name)
		throws UnknownHostException, MongoException {
		
		AccountDAO dao = AccountDAO.singleton();
		Account account = dao.findOne("_id", new ObjectId(id));
		
		if (account == null) {
			throw new InputParamsException();
		}
		
		if (!Account.validateName(name)) {
			throw new JSONException(400, "Invalid name!");
		}
		
		account.setName(name);
		dao.save(account);
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response add(JAXBElement<Account> account) {
		Account a = account.getValue();
		return putAndGetResponse(a);
	}
	
	@DELETE
	public String delete() throws UnknownHostException, MongoException {
		TransactionDAO dao = TransactionDAO.singleton();
		if (dao.exists(dao.createQuery().filter("account_id", new ObjectId(id)))) {
			Account account = AccountDAO.singleton().findOne("_id", new ObjectId(id));
			
			if (account == null) {
				throw new InputParamsException();
			}
			
			account.setHidden(true);
			AccountDAO.singleton().save(account);
		} else {
			WriteResult r = AccountDAO.singleton().deleteById(new ObjectId(id));
			
			if (r == null) {
				throw new InputParamsException();
			}
		}
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	private Response putAndGetResponse(Account account) {
		Response res = null;
	//	if(TransactionDAO.singleton().containsKey(transaction.getId())) {
	//		res = Response.noContent().build();
	//	} else {
	//		res = Response.created(uriInfo.getAbsolutePath()).build();
	//	}
	//	TodoDao.instance.getModel().put(transaction.getId(), transaction);
		return res;
	}
}
