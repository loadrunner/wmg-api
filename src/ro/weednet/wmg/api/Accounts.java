package ro.weednet.wmg.api;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import com.mongodb.MongoException;

import ro.weednet.common.InputParamsException;
import ro.weednet.common.JSONException;
import ro.weednet.common.SessionException;
import ro.weednet.wmg.dao.AccountDAO;
import ro.weednet.wmg.dao.SessionDAO;
import ro.weednet.wmg.dao.TransactionDAO;
import ro.weednet.wmg.model.Account;
import ro.weednet.wmg.model.Session;
import ro.weednet.wmg.model.Transaction;
import ro.weednet.wmg.model.User;
import ro.weednet.wmg.rest.AccountResource;

@Path("/accounts")
public class Accounts {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public ArrayList<Account> list(
		@QueryParam("session_id") String session_id,
		@QueryParam("include_hidden") @DefaultValue("false") boolean include_hidden,
		@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		ArrayList<Account> list;
		
		list = user.getAccounts(include_hidden);
		
		return list;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String add(
			@FormParam("name") String name,
			@FormParam("currency") String currency,
			@FormParam("initial_balance") @DefaultValue("0") double initial_balance,
			@QueryParam("session_id") String session_id,
			@Context HttpServletResponse servletResponse
	) throws IOException {
		//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		if (!Account.validateName(name)) {
			throw new JSONException(400, "Invalid name!");
		}
		
		User user = session.getUser();
		
		Account account = new Account(user, name, currency);
		
		AccountDAO.singleton().save(account);
		
		if (initial_balance != 0) {
			Transaction transaction = new Transaction(
				user,
				account,
				initial_balance > 0 ? Transaction.TYPE_DEPOSIT : Transaction.TYPE_WITHDRAWAL,
				Math.abs(initial_balance),
				1,
				Math.abs(initial_balance),
				account.getCurrency(),
				"initial balance",
				(int) System.currentTimeMillis() / 1000
			);
			TransactionDAO.singleton().save(transaction);
		}
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("account_id", account.getId().toString());
		r.put("message", "good");
		
		return r.toString();
	}
	
	@Path("{account}")
	public AccountResource getAccount(
			@PathParam("account") String id,
			@QueryParam("session_id") String session_id
		) throws UnknownHostException, MongoException {
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		Account account = AccountDAO.singleton().findOne("_id", new ObjectId(id));
		
		if (account == null || !account.getUserId().equals(user.getId())) {
			throw new InputParamsException();
		}
		
		return new AccountResource(uriInfo, request, id);
	}
}