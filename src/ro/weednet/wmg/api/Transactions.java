package ro.weednet.wmg.api;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import com.google.code.morphia.query.Query;
import com.mongodb.MongoException;

import ro.weednet.common.InputParamsException;
import ro.weednet.common.JSONException;
import ro.weednet.common.SessionException;
import ro.weednet.wmg.dao.AccountDAO;
import ro.weednet.wmg.dao.TransactionDAO;
import ro.weednet.wmg.dao.SessionDAO;
import ro.weednet.wmg.model.Account;
import ro.weednet.wmg.model.Session;
import ro.weednet.wmg.model.Transaction;
import ro.weednet.wmg.model.User;
import ro.weednet.wmg.rest.TransactionResource;

@Path("/transactions")
public class Transactions {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public ArrayList<Transaction> list(
		@QueryParam("session_id") String session_id,
		@QueryParam("account_id") String account_id,
		@QueryParam("offset") @DefaultValue("0") int offset,
		@QueryParam("keywords") String keywords,
		@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0
		 || offset < 0 || offset > 10000) {
			throw new InputParamsException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		Account account = null;
		ArrayList<Transaction> list;
		
		if (account_id != null && account_id.length() > 0)
		{
			AccountDAO dao = AccountDAO.singleton();
			account = dao.findOne("_id", new ObjectId(account_id));
			
			if (account == null || !account.getUserId().equals(user.getId())) {
				throw new JSONException(400, "invalid account");
			}
		}
		else if (keywords != null && keywords.length() > 0)
		{
			String regEx = "@([a-zA-Z]+[a-zA-Z0-9-_]*[a-zA-Z]+)";
			Pattern accountPattern = Pattern.compile(regEx);
			Matcher m = accountPattern.matcher(keywords);
			String accountName = null;
			
			if (m.find()) {
				accountName = m.group(1);
				
				AccountDAO dao = AccountDAO.singleton();
				Query<Account> query = dao.createQuery();
				query.filter("user_id", user.getId());
				query.filter("name", accountName);
				account = dao.findOne(query);
				
				if (account == null || !account.getUserId().equals(user.getId())) {
					throw new JSONException(400, "invalid account");
				}
			}
			
			keywords = accountPattern.matcher(keywords).replaceAll("");
		}
		
		if (account == null) {
			list = user.getTransactions(keywords, offset);
		} else {
			list = account.getTransactions(keywords, offset);
		}
		
		return list;
	}
	
	@GET
	@Path("stats")
	@Produces({MediaType.APPLICATION_JSON})
	public String stats(
		@QueryParam("session_id") String session_id,
		@QueryParam("keywords") String keywords,
		@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
		if (session_id == null || session_id.length() == 0) {
			throw new InputParamsException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		
		int interval = Calendar.DATE,
			period = 30;
		
		Calendar date = Calendar.getInstance();
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.add(interval, -period);
		
		//TODO: fix currencies
		BSONObject stats = Transaction.getStats(user, keywords, date, interval, period);
		
		if (stats == null) {
			throw new InputParamsException();
		}
		
		return stats.toString();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String add(
			@QueryParam("session_id") String session_id,
			@FormParam("account_id") String account_id,
			@FormParam("description") String description,
			@FormParam("type") byte type,
			@FormParam("value") double value,
			@FormParam("rate") double rate,
			@FormParam("total") double total,
			@FormParam("currency") String currency,
			@FormParam("timestamp") int timestamp,
			@Context HttpServletResponse servletResponse
	) throws IOException {
		//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new InputParamsException();
		}
		
		Account account = AccountDAO.singleton().findOne("_id", new ObjectId(account_id));
		User user = session.getUser();
		
		if (account == null || !account.getUserId().equals(user.getId())) {
			throw new InputParamsException();
		}
		
		Transaction transaction = new Transaction(
			user,
			account,
			type,
			value,
			rate,
			total,
			currency,
			description,
			timestamp
		);
		TransactionDAO.singleton().save(transaction);
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	@PUT
	@Path("transfer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String transfer(
			@QueryParam("session_id") String session_id,
			@FormParam("src_account_id") String src_account_id,
			@FormParam("dst_account_id") String dst_account_id,
			@FormParam("description") String description,
			@FormParam("src_value") double src_value,
			@FormParam("dst_value") double dst_value,
			@FormParam("timestamp") int timestamp,
			@Context HttpServletResponse servletResponse
	) throws IOException {
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new InputParamsException();
		}
		
		User user = session.getUser();
		Account src_account = AccountDAO.singleton().findOne("_id", new ObjectId(src_account_id));
		Account dst_account = AccountDAO.singleton().findOne("_id", new ObjectId(dst_account_id));
		
		if (src_account == null || !src_account.getUserId().equals(user.getId())
		 || dst_account == null || !dst_account.getUserId().equals(user.getId())) {
			throw new InputParamsException();
		}
		
		double src_rate = 1;
		double dst_rate = 1;
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		try {
			src_rate = Double.parseDouble(twoDForm.format(src_value / dst_value));
		} catch (Exception ex) { }
		try {
			dst_rate = Double.parseDouble(twoDForm.format(dst_value / src_value));
		} catch (Exception ex) { }
		
		Transaction src_transaction = new Transaction(
			user,
			src_account,
			Transaction.TYPE_WITHDRAWAL,
			dst_value,
			src_rate,
			src_value,
			dst_account.getCurrency(),
			"#transfer " + description,
			timestamp
		);
		TransactionDAO.singleton().save(src_transaction);
		
		Transaction dst_transaction = new Transaction(
			user,
			dst_account,
			Transaction.TYPE_DEPOSIT,
			src_value,
			dst_rate,
			dst_value,
			src_account.getCurrency(),
			"#transfer " + description,
			timestamp
		);
		TransactionDAO.singleton().save(dst_transaction);
		
		src_transaction.setTwin(dst_transaction);
		TransactionDAO.singleton().save(src_transaction);
		
		dst_transaction.setTwin(src_transaction);
		TransactionDAO.singleton().save(dst_transaction);
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	@Path("{transaction}")
	public TransactionResource getTransaction(
			@PathParam("transaction") String id,
			@QueryParam("session_id") String session_id
		) throws UnknownHostException, MongoException {
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		Transaction transaction = TransactionDAO.singleton().findOne("_id", new ObjectId(id));
		
		if (transaction == null || !transaction.getUserId().equals(user.getId())) {
			throw new InputParamsException();
		}
		
		return new TransactionResource(uriInfo, request, id);
	}
}