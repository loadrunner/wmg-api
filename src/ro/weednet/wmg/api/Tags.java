package ro.weednet.wmg.api;

import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.mongodb.MongoException;

import ro.weednet.common.SessionException;
import ro.weednet.wmg.dao.SessionDAO;
import ro.weednet.wmg.model.Session;
import ro.weednet.wmg.model.Tag;
import ro.weednet.wmg.model.User;
import ro.weednet.wmg.rest.TagResource;

@Path("/tags")
public class Tags {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public ArrayList<Tag> list(
		@QueryParam("session_id") String session_id,
		@QueryParam("include_empty") @DefaultValue("false") boolean include_empty,
		@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		ArrayList<Tag> list = user.getTags(include_empty);
		
		return list;
	}
	
	@Path("{tag}")
	public TagResource getTag(
			@PathParam("tag") String tag,
			@QueryParam("session_id") String session_id
		) throws UnknownHostException, MongoException {
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		return new TagResource(uriInfo, request, session.getUser(), tag);
	}
}