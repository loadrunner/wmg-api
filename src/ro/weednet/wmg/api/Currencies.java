package ro.weednet.wmg.api;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.bson.BasicBSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.mongodb.MongoException;

import ro.weednet.wmg.dao.CurrencyDAO;
import ro.weednet.wmg.model.Currency;

@Path("/currencies")
public class Currencies {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public ArrayList<Currency> list(
		@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
		
		CurrencyDAO dao = CurrencyDAO.singleton();
		
		Iterable<Currency> i = dao.find().fetch();
		ArrayList<Currency> list = new ArrayList<Currency>();
		
		for(Currency currency: i) {
			list.add(currency);
		}
		
		return list;
	}
	
	@Path("update")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String update(@Context HttpServletResponse servletResponse) throws IOException {
		//TODO: security
		
		CurrencyDAO dao = CurrencyDAO.singleton();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		String code;
		double rate;
		Currency currency;
		
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			Document dom = db.parse("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
			Element docEle = dom.getDocumentElement();
			NodeList nl = docEle.getElementsByTagName("Cube");
			
			if(nl != null && nl.getLength() > 0) {
				for (int i = 0; i < nl.getLength(); i++) {
					
					//get the employee element
					Element el = (Element) nl.item(i);
					
					code = el.getAttribute("currency");
					
					if (code == null || code.length() == 0) {
						continue;
					}
					
					try {
						rate = Double.parseDouble(el.getAttribute("rate"));
					} catch (NumberFormatException e) {
						rate = 1;
					}
					
					currency = dao.findOne("code", code);
					if (currency == null) {
						continue;
					//	currency = new Currency(code, rate);
					} else {
						currency.setRate(rate);
					}
					dao.save(currency);
				}
			}
			
			//System.out.println(nl.getLength());
		} catch (Exception e) {
			System.out.println("error " + e.getMessage());
		}
		
		currency = dao.findOne("code", "MSP");
		if (currency != null)
		{
			currency.setRate(80 * Currency.getRate("EUR", "USD"));
			dao.save(currency);
		}
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "done");
		
		return r.toString();
	}
}