package ro.weednet.wmg.api;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import com.mongodb.MongoException;

import ro.weednet.common.InputParamsException;
import ro.weednet.common.JSONException;
import ro.weednet.common.SessionException;
import ro.weednet.wmg.dao.BudgetDAO;
import ro.weednet.wmg.dao.SessionDAO;
import ro.weednet.wmg.model.Budget;
import ro.weednet.wmg.model.Session;
import ro.weednet.wmg.model.Tag;
import ro.weednet.wmg.model.User;
import ro.weednet.wmg.rest.BudgetResource;

@Path("/budgets")
public class Budgets {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public ArrayList<Budget> list(
		@QueryParam("session_id") String session_id,
		@QueryParam("include_hidden") @DefaultValue("false") boolean include_hidden,
		@QueryParam("type") @DefaultValue("0") byte type,
		@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		ArrayList<Budget> list = user.getBudgets(type, include_hidden);
		
		return list;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String add(
			@FormParam("name") String name,
			@FormParam("tag") String tag,
			@FormParam("value") double value,
			@FormParam("length") int length,
			@FormParam("type") byte type,
			@FormParam("currency") String currency,
			@QueryParam("session_id") String session_id,
			@Context HttpServletResponse servletResponse
	) throws IOException {
		//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (session_id == null || session_id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		if (!Budget.validateName(name)) {
			throw new JSONException(400, "Invalid name!");
		}
		if (!Tag.validateName(tag)) {
			throw new JSONException(400, "Invalid tag name!");
		}
		if (!Budget.validateValue(value)) {
			throw new JSONException(400, "Invalid value!");
		}
		if (!Budget.validateLength(length)) {
			throw new JSONException(400, "Invalid length!");
		}
		if (!Budget.validateType(type)) {
			throw new JSONException(400, "Invalid type!");
		}
		if (!Budget.validateCurrency(currency)) {
			throw new JSONException(400, "Invalid currency!");
		}
		
		User user = session.getUser();
		
		Budget budget = new Budget(user.getId(), name, new Tag(tag), value, currency, length, type);
		
		BudgetDAO.singleton().save(budget);
		
		BasicBSONObject r = new BasicBSONObject();
		
		r.put("code", "200");
		r.put("message", "good");
		
		return r.toString();
	}
	
	@Path("{budget}")
	public BudgetResource getBudget(
			@PathParam("budget") String id,
			@QueryParam("session_id") String session_id
		) throws UnknownHostException, MongoException {
		Session session = SessionDAO.singleton().findOne("hash", session_id);
		
		if (session == null) {
			throw new SessionException();
		}
		
		User user = session.getUser();
		Budget budget = BudgetDAO.singleton().findOne("_id", new ObjectId(id));
		
		if (budget == null || !budget.getUserId().equals(user.getId())) {
			throw new InputParamsException();
		}
		
		return new BudgetResource(uriInfo, request, id);
	}
}