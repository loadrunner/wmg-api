package ro.weednet.wmg.api;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import com.google.code.morphia.query.Query;
import com.mongodb.MongoException;

import ro.weednet.common.JSONException;
import ro.weednet.common.SessionException;
import ro.weednet.wmg.Server;
import ro.weednet.wmg.dao.SessionDAO;
import ro.weednet.wmg.dao.UserDAO;
import ro.weednet.wmg.model.Session;
import ro.weednet.wmg.model.User;

@Path("/auth")
public class Auth {
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	
	@PUT
	@Path("register")
	@Produces(MediaType.APPLICATION_JSON)
	public String register(
			@FormParam("email") String email,
			@FormParam("password") String rpwd,
			@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (email == null || email.length() == 0
		 || rpwd == null || rpwd.length() == 0) {
			throw new JSONException(400, "invalid email and/or password");
		}
		
		String pwd = User.create_password_hash(rpwd);
		
		UserDAO dao = UserDAO.singleton();
		User user = dao.findOne(dao.createQuery().filter("email", email));
		
		if (user == null) {
			user = new User();
			
			user.setEmail(email);
			user.setPassword(pwd);
			dao.save(user);
			
			//first auto account
		//	Account account = new Account(user, "Wallet", "EUR");
		//	AccountDAO.singleton().save(account);
			
			Session session = user.createAccessSession();
			
			BasicBSONObject r = new BasicBSONObject();
			r.put("code", 200);
			r.put("user_id", user.getId().toString());
			r.put("session_id", session.getHash());
			
			return r.toString();
		} else {
			throw new JSONException(400, "email already exists in the database");
		}
	}
	
	@GET
	@Path("request")
	@Produces(MediaType.APPLICATION_JSON)
	public String request(
			@QueryParam("email") String email,
			@QueryParam("password") String rpwd,
			@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (email == null || email.length() == 0
		 || rpwd == null || rpwd.length() == 0) {
			throw new JSONException(400, "invalid email and/or password");
		}
		
		String pwd = User.create_password_hash(rpwd);
		
		UserDAO dao = UserDAO.singleton();
		User user = dao.findOne(dao.createQuery().filter("email", email).filter("password", pwd));
		
		if (user != null) {
			Session session = user.createAccessSession();
			
			BasicBSONObject r = new BasicBSONObject();
			r.put("code", 200);
			r.put("user_id", user.getId().toString());
			r.put("session_id", session.getHash());
			
			return r.toString();
		} else {
			throw new JSONException(400, "wrong email and/or password");
		}
	}
	
	@GET
	@Path("check_session")
	@Produces(MediaType.APPLICATION_JSON)
	public String check(
			@QueryParam("id") String id,
			@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
	//	try { Thread.sleep(2000); } catch (InterruptedException e) {}
		if (id == null || id.length() == 0) {
			throw new SessionException();
		}
		
		Session session = SessionDAO.singleton().findOne("hash", id);
		
		if (session != null) {
			
			BasicBSONObject r = new BasicBSONObject();
			r.put("code", "200");
			r.put("user_id", session.getUser().getIdSring());
			r.put("message", "good");
			
			return r.toString();
		} else {
			throw new SessionException();
		}
	}
	
	@GET
	@Path("recover")
	@Produces(MediaType.APPLICATION_JSON)
	public String recover(
			@QueryParam("email") String email,
			@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
		if (email == null || email.length() == 0) {
			throw new JSONException(400, "invalid email");
		}
		Server server = Server.singleton();
		
		UserDAO dao = UserDAO.singleton();
		User user = dao.findOne(dao.createQuery().filter("email", email));
		
		if (user != null) {
			
			try{
				String hash = User.create_password_hash(user.getIdSring() + Math.random() + System.currentTimeMillis());
				user.setRecoverHash(hash);
				dao.save(user);
				
				Address from;
				try {
					from = new InternetAddress(server.getConfig("default_email"), "WMG Support", "UTF8");
				} catch (UnsupportedEncodingException e) {
					from = new InternetAddress(server.getConfig("default_email"));
					e.printStackTrace();
				}
				Address to = new InternetAddress(user.getEmail());
				String subject = "Recover password";
				String body = "Click on this link "
					+ server.getConfig("webapp_url")
					+ "/logging/recover_form"
					+ "?id=" + user.getIdSring()
					+ "&hash=" + hash;
				
				Properties properties = System.getProperties();
				properties.setProperty("mail.smtp.host", server.getConfig("smtp.host"));
				javax.mail.Session session = javax.mail.Session.getDefaultInstance(properties);
				
				MimeMessage message = new MimeMessage(session);
				message.setFrom(from);
				message.addRecipient(Message.RecipientType.TO, to);
				message.setSubject(subject);
				message.setText(body);
				Transport.send(message);
				
				BasicBSONObject r = new BasicBSONObject();
				r.put("code", 200);
				return r.toString();
			} catch (MessagingException mex) {
				mex.printStackTrace();
				
				BasicBSONObject r = new BasicBSONObject();
				r.put("code", 500);
				return r.toString();
			}
		} else {
			throw new JSONException(400, "wrong email");
		}
	}
	
	@POST
	@Path("change_password")
	@Produces(MediaType.APPLICATION_JSON)
	public String change_password(
			@FormParam("id") String id,
			@FormParam("hash") String hash,
			@FormParam("password") String rpwd,
			@Context HttpServletResponse response
	) throws UnknownHostException, MongoException {
		if (id == null || id.length() == 0
		 || hash == null || hash.length() == 0
		 || rpwd == null || rpwd.length() < 4) {
			throw new JSONException(400, "invalid params"+ id + hash);
		}
		
		String pwd = User.create_password_hash(rpwd);
		
		UserDAO dao = UserDAO.singleton();
		User user = dao.findOne(dao.createQuery().filter("_id", new ObjectId(id)));
		
		if (user != null && hash.equals(user.getRecoverHash())) {
			user.setRecoverHash(null);
			user.setPassword(pwd);
			dao.save(user);
			
			SessionDAO sessionDao = SessionDAO.singleton();
			Query<Session> query = sessionDao.createQuery().filter("user_id", user.getId());
			sessionDao.deleteByQuery(query);
			
			BasicBSONObject r = new BasicBSONObject();
			r.put("code", 200);
			r.put("user_id", user.getId().toString());
			
			return r.toString();
		} else {
			throw new JSONException(400, "cannot find user with supplied parameters");
		}
	}
}