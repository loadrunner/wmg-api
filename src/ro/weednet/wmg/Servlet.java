package ro.weednet.wmg;

import javax.servlet.ServletException;

import com.sun.jersey.spi.container.servlet.ServletContainer;

public class Servlet extends ServletContainer {
	private static final long serialVersionUID = 6323632100217630578L;
	
	@Override
	public void init() {
		try {
			super.init();
		} catch (ServletException e) {
			
		}
		
		Server.init();
		Server.singleton();
	}
}
