package ro.weednet.wmg.model;

import java.math.BigInteger;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.bson.types.ObjectId;

import ro.weednet.common.JSONException;
import ro.weednet.wmg.dao.UserDAO;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.mongodb.MongoException;

@XmlRootElement(name = "Session")
@Entity("sessions")
public class Session {
	@Id
	private ObjectId _id;
	private ObjectId user_id;
	private String hash;
	private int date;
	
	public Session() {
		this((ObjectId)null);
	}
	public Session(User user) {
		this(user.getId());
	}
	public Session(String uid) {
		this(new ObjectId(uid));
	}
	public Session(ObjectId uid) {
		user_id = uid;
		date = (int) (System.currentTimeMillis() / 1000);
	}
	
	public ObjectId getId() {
		return _id;
	}
	@XmlElement(name = "id")
	public String getIdSring() {
		return _id.toString();
	}
	public String getHash() {
		return hash;
	}
	public long getDate() {
		return date;
	}
	
	public User getUser() {
		User user = null;
		
		try {
			UserDAO dao = UserDAO.singleton();
			user = dao.findOne("_id", user_id);
		} catch (UnknownHostException | MongoException e) {
			
		}
		
		if (user == null) {
			throw new JSONException(500, "Internal Error");
		}
		
		return user;
	}
	
	public void generateHash() {
		hash = "xx_" + getUser().getIdSring() + "_" + (getDate() * Math.random()) + "_" + "_yy";
		try {
			MessageDigest md5;
			md5 = MessageDigest.getInstance("MD5");
			md5.update(hash.getBytes(), 0, hash.length());
			hash = (new BigInteger(1,md5.digest())).toString(16);
		} catch (NoSuchAlgorithmException e) { }
	}
}