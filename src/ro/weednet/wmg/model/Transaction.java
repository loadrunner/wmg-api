package ro.weednet.wmg.model;

import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import ro.weednet.common.Pair;
import ro.weednet.wmg.dao.AccountDAO;
import ro.weednet.wmg.dao.TransactionDAO;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.query.Query;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.MongoException;

@XmlRootElement(name = "Transaction")
@Entity("transactions")
public class Transaction {
	public static final byte TYPE_DEPOSIT = 1 << 0;
	public static final byte TYPE_WITHDRAWAL = 1 << 1;
	
	@Id
	private ObjectId _id;
	private ObjectId user_id;
	private ObjectId account_id;
	private ObjectId twin_id;
	private String description;
	@Embedded
	private ArrayList<Tag> tags;
	private byte type = 1;
	private double value = 0;
	private double rate = 1;
	private double total = 0;
	private String currency;
	private int timestamp;
	private int created_at;
	private int modified_at;
	
	public Transaction() {
		
	}
	public Transaction(User user, Account account, byte type, double value, double rate, double total, String currency, String description, int timestamp) {
		setUserId(user.getId());
		setAccountId(account.getId());
		setType(type);
		setValue(value);
		setRate(rate);
		setTotal(total);
		setCurrency(currency);
		setDescription(description);
		setTimestamp(timestamp);
		created_at = (int) (System.currentTimeMillis() / 1000);
	}
	
	public ObjectId getId() {
		return _id;
	}
	@XmlElement(name = "id")
	public String getIdSring() {
		return _id.toString();
	}
	@XmlTransient
	public ObjectId getUserId() {
		return user_id;
	}
	@XmlElement(name = "user_id")
	public String getUserIdString() {
		return user_id.toString();
	}
	@XmlElement(name = "account_id")
	public String getAccountId() {
		return account_id.toString();
	}
	@XmlElement(name = "twin_id")
	public String getTwinId() {
		return (twin_id != null) ? twin_id.toString() : null;
	}
	@XmlTransient
	public Transaction getTwin() {
		if (twin_id == null) {
			return null;
		}
		
		try {
			return TransactionDAO.singleton().findOne("_id", twin_id);
		} catch (UnknownHostException | MongoException e) {
			return null;
		}
	}
	public String getDescription() {
		return description;
	}
	public byte getType() {
		return type;
	}
	public double getValue() {
		return value;
	}
	public double getRate() {
		return rate;
	}
	@XmlElement(name = "total")
	public double getTotal() {
		return total;
	}
	public String getCurrency() {
		return currency;
	}
	public int getTimestamp() {
		return timestamp;
	}
	public int getCreatedAt() {
		return created_at;
	}
	public int getModifiedAt() {
		return modified_at;
	}
	@XmlElement(name = "tags")
	public ArrayList<Tag> getTags() {
		return tags;
	}
	public boolean isDeposit() {
		return type == TYPE_DEPOSIT;
	}
	public boolean isWhithdrawal() {
		return type == TYPE_WITHDRAWAL;
	}
	@XmlElement(name = "account")
	public Account getAccount() {
		try {
			return AccountDAO.singleton().findOne("_id", new ObjectId(getAccountId()));
		} catch (Exception e) {
			return null;
		}
	}
	
	public void setDescription(String value) {
		Pair<String, ArrayList<Tag>> result = parseDescription(value);
		
		description = result.first;
		if (result.second.size() > 0) {
			if (tags == null) {
				tags = new ArrayList<Tag>();
			}
			tags.addAll(result.second);
		}
	}
	public void setTags(ArrayList<Tag> values) {
		tags = values;
	}
	public void setType(byte value) {
		type = value;
	}
	public void setValue(double value) {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		this.value = Double.valueOf(twoDForm.format(value));
	}
	public void setRate(double value) {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		rate = Double.valueOf(twoDForm.format(value));
	}
	public void setTotal(double value) {
		if (type == TYPE_WITHDRAWAL) {
			value = -value;
		}
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		total = Double.valueOf(twoDForm.format(value));
	}
	public void setCurrency(String value) {
		currency = value;
	}
	public void setTimestamp(int value) {
		timestamp = value;
	}
	public void setUserId(String value) {
		user_id = new ObjectId(value);
	}
	public void setUserId(ObjectId value) {
		user_id = value;
	}
	public void setAccountId(String value) {
		account_id = new ObjectId(value);
	}
	public void setAccountId(ObjectId value) {
		account_id = value;
	}
	public void setTwin(Transaction transcation) {
		this.twin_id = transcation.getId();
	}
	
	public static Pair<String, ArrayList<Tag>> parseDescription(String value) {
		Pattern p = Pattern.compile("\\ *#" + Tag.NAME_REGEX + "\\ *");
		Matcher m = p.matcher(value);
		
		ArrayList<Tag> tags = new ArrayList<Tag>();
		
		int start = 0;
		while (m.find(start)) {
			tags.add(new Tag(value.substring(m.start(), m.end()).replace("#","").trim()));
			start = m.end();
		}
		
		String description = value.replaceAll("\\ *#" + Tag.NAME_REGEX + "\\ *", " ").trim();
		
		return new Pair<String, ArrayList<Tag>>(description, tags);
	}
	public static BSONObject processKeywords(BSONObject query, String keywords) {
		if (keywords != null && keywords.length() > 0) {
			Pair<String, ArrayList<Tag>> r = Transaction.parseDescription(keywords);
			if (r.first != null && r.first.length() > 0) {
				String[] words = r.first.split("([ ]+)");
				String regex = "";
				for (int i = 0; i < words.length; i++) {
					if (words[i].length() > 0) {
						if (regex.length() > 0) {
							regex += "(.*)?";
						}
						regex += Pattern.quote(words[i]);
					}
				}
				
				if (regex.length() > 0) {
					query.put("description", Pattern.compile(regex,Pattern.CASE_INSENSITIVE));
				}
			}
			
			if (r.second != null && r.second.size() > 0) {
				for (int i = 0; i < r.second.size(); i++) {
					query.put("tags.name", r.second.get(i).getName());
				}
			}
		}
		
		return query;
	}
	public static Query<Transaction> processKeywords(Query<Transaction> query, String keywords) {
		BSONObject tmp_query = new BasicBSONObject();
		processKeywords(tmp_query, keywords);
		
		Iterator<String> it = tmp_query.keySet().iterator();
		String key;
		while (it.hasNext()) {
			key = it.next();
			query.filter(key, tmp_query.get(key));
		}
		return query;
	}
	public static BSONObject getStats(User user, String keywords, Calendar start_date, int interval, int period) {
	//	{initial: {balance: {}}, reduce: function (o, p) { var date = new Date(o.timestamp * 1000); date = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(); p[date] = p[date] || 0; p[date] += o.total;}}
		BSONObject stats = new BasicBSONObject();
		Calendar tmp = (Calendar) start_date.clone();
		for (int i = 1; i <= period; i++) {
			String date  = tmp.get(Calendar.YEAR) + "-" + (tmp.get(Calendar.MONTH) + 1) + "-" + tmp.get(Calendar.DAY_OF_MONTH);
			stats.put(date, 0);
			tmp.add(interval, 1);
		}
		
		try {
			
			//old: function (o, p) { var key = Math.ceil((o.timestamp - " + startTime + ") / 86400); p.balance[key] = p.balance[key] || 0; p.balance[key] += o.total;}
			//new v1: db.transactions.group({initial: {balance: {}}, reduce: function (o, p) { var d = new Date(o.timestamp * 1000), key = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate(); p.balance[key] = p.balance[key] || 0; p.balance[key] += o.total; }});
			//new v2: db.transactions.group({initial: {balance: 0}, keyf: function (o) {var d = new Date(o.timestamp * 1000); return {"date": d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() };}, reduce: function (o, p) { p.balance += o.total; }});
			DBCollection tran = TransactionDAO.singleton().getCollection();
			
			BSONObject query = new BasicBSONObject();
			query.put("user_id", user.getId());
			processKeywords(query, keywords);
			query.put("timestamp", new BasicDBObject("$gte", (int) (start_date.getTimeInMillis() / 1000)));
			
			BasicDBObject initial = new BasicDBObject();
			initial.put("balance", 0);
			
			String reduce = "function (o, p) { p.balance += o.total; }";
			String keyf = "function (o) {var d = new Date(o.timestamp * 1000); return {\"date\": d.getFullYear() + \"-\" + (d.getMonth() + 1) + \"-\" + d.getDate() };}";
			BasicDBObject args  = new BasicDBObject();
			args.put("initial", initial);
			args.put("cond", query);
			args.put("$reduce", reduce);
			args.put("$keyf", keyf);
			
			@SuppressWarnings("deprecation")
			BasicDBList result = (BasicDBList) tran.group(args);
			
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			for (int i = 0; i < result.size(); i++) {
				BasicBSONObject r = (BasicBSONObject) result.get(i);
				Double dd = r.getDouble("balance");
				stats.put((String) r.get("date"), Double.valueOf(twoDForm.format(dd)));
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		return stats;
	}
}