package ro.weednet.wmg.model;

import java.math.BigInteger;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.bson.types.ObjectId;

import ro.weednet.wmg.dao.AccountDAO;
import ro.weednet.wmg.dao.BudgetDAO;
import ro.weednet.wmg.dao.SessionDAO;
import ro.weednet.wmg.dao.TransactionDAO;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.query.Query;
import com.mongodb.MongoException;

@XmlRootElement(name = "User")
@Entity("users")
public class User {
	@Id
	private ObjectId _id;
	private String name;
	@SuppressWarnings("unused")
	private String password;
	private String email;
	private String recover_hash;
	
	public User() {
		
	}
	
	public ObjectId getId() {
		return _id;
	}
	@XmlElement(name = "id")
	public String getIdSring() {
		return _id.toString();
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getRecoverHash() {
		return recover_hash;
	}
	
	public Session createAccessSession() throws UnknownHostException, MongoException {
		Session session = new Session(this);
		session.generateHash();
		
		SessionDAO.singleton().save(session);
		
		return session;
	}
	public ArrayList<Account> getAccounts(boolean include_hidden) {
		ArrayList<Account> accounts = new ArrayList<Account>();
		
		try {
			AccountDAO dao = AccountDAO.singleton();
			
			Query<Account> query = dao.createQuery().filter("user_id", getId());
			if (!include_hidden) {
				query = query.filter("is_hidden !=", true);
			}
			query.order("name");
			
			Iterable<Account> i = dao.find(query).fetch();
			
			for(Account account: i) {
				account.calculateBalance();
				accounts.add(account);
			}
		} catch (UnknownHostException | MongoException e) {
			
		}
		
		return accounts;
	}
	public ArrayList<Budget> getBudgets(byte type, boolean include_hidden) {
		ArrayList<Budget> budgets = new ArrayList<Budget>();
		
		try {
			BudgetDAO dao = BudgetDAO.singleton();
			
			Query<Budget> q = dao.createQuery().filter("user_id", getId());
			if (Budget.validateType(type)) {
				q.filter("type", type);
			}
			q.order("name");
			
			Iterable<Budget> i = dao.find(q).fetch();
			
			for(Budget budget: i) {
				budget.processData();
				budgets.add(budget);
			}
		} catch (UnknownHostException | MongoException e) {
			
		}
		
		return budgets;
	}
	public ArrayList<Transaction> getTransactions() {
		return getTransactions(null, 0);
	}
	public ArrayList<Transaction> getTransactions(String keywords, int offset) {
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		
		try {
			TransactionDAO dao = TransactionDAO.singleton();
			Query<Transaction> query = dao.createQuery();
			query.filter("user_id", getId());
			Transaction.processKeywords(query, keywords);
			query.order("-timestamp, -_id");
			if (offset > 0) {
				query.offset(offset).limit(25);
			} else {
				query.limit(50);
			}
			Iterable<Transaction> i = dao.find(query).fetch();
			
			for(Transaction transaction: i) {
				transactions.add(transaction);
			}
		} catch (UnknownHostException | MongoException e) {
			
		}
		
		return transactions;
	}
	public ArrayList<Tag> getTags(boolean include_empty) {
		ArrayList<Tag> tags = new ArrayList<Tag>();
		/*
		try {
			TransactionDAO dao = TransactionDAO.singleton();
			BasicDBObject query = new BasicDBObject().append("user_id", this.getId());
			@SuppressWarnings("unchecked")
			List<BasicDBObject> list = dao.getCollection().distinct("tags", query);
			System.out.println(list.toString());
			Iterator<BasicDBObject> itr = list.iterator();
			while(itr.hasNext()) {
				BasicDBObject element = itr.next();
				tags.add(new Tag(element, this.getId()));
			} 
		} catch (UnknownHostException | MongoException e) {
			
		}
		*/
		return tags;
	}
	
	public void setName(String value) {
		name = value;
	}
	public void setPassword(String value) {
		password = value;
	}
	public void setEmail(String value) {
		email = value;
	}
	public void setRecoverHash(String value) {
		recover_hash = value;
	}
	
	public static String create_password_hash(String value) {
		String pwd = null;
		try {
			MessageDigest md5;
			md5 = MessageDigest.getInstance("MD5");
			md5.update(value.getBytes(), 0, value.length());
			pwd = (new BigInteger(1,md5.digest())).toString(16);
		} catch (NoSuchAlgorithmException e) {
			
		}
		return pwd;
	}
}