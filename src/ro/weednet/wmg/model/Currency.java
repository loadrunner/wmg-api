package ro.weednet.wmg.model;

import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.bson.types.ObjectId;

import ro.weednet.wmg.dao.CurrencyDAO;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.mongodb.MongoException;

@XmlRootElement(name = "Currency")
@Entity("currencies")
public class Currency {
	@Id
	private ObjectId _id;
	private String code;
	private String name;
	private double rate;
	
	public Currency() {
		this(null, 1);
	}
	public Currency(String code, double rate) {
		this(code, rate, "");
	}
	public Currency(String code, double rate, String name) {
		setCode(code);
		setRate(rate);
		setName(name);
	}
	
	
	public ObjectId getId() {
		return _id;
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public double getRate() {
		return rate;
	}
	
	public void setCode(String value) {
		code = value;
	}
	public void setName(String value) {
		name = value;
	}
	public void setRate(double value) {
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		rate = Double.valueOf(twoDForm.format(value));
	}
	
	public static double getRate(String from, String to) throws UnknownHostException, MongoException {
		Map<String, Currency> currencies = new HashMap<String, Currency>();
		
		CurrencyDAO dao = CurrencyDAO.singleton();
		Iterable<Currency> i = dao.find().fetch();
		
		for (Currency currency: i) {
			currencies.put(currency.getCode(), currency);
		}
		
		if (from.equals("EUR")) {
			if (currencies.containsKey(to)) {
				return currencies.get(to).getRate();
			}
		} else if (to.equals("EUR")) {
			if (currencies.containsKey(from)) {
				DecimalFormat twoDForm = new DecimalFormat("#.##");
				return Double.valueOf(twoDForm.format(1/currencies.get(from).getRate()));
			}
		} else if (currencies.containsKey(from) && currencies.containsKey(to)) {
			Currency c1 = currencies.get(from);
			Currency c2 = currencies.get(to);
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			return Double.valueOf(twoDForm.format(1 / c1.getRate() * c2.getRate()));
		}
		
		return 1;
	}
}