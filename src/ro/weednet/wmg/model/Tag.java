package ro.weednet.wmg.model;

import java.text.DecimalFormat;
import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.types.ObjectId;

import ro.weednet.wmg.dao.TransactionDAO;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.NotSaved;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

@XmlRootElement(name = "Tag")
@Entity("tags")
public class Tag {
	public static final String NAME_REGEX = "([a-zA-Z0-9]+)";
	
	private String name;
	@NotSaved
	private ObjectId user_id;
	
	public Tag() {
		this((String) null);
	}
	public Tag(String name) {
		setName(name);
	}
	public Tag(String name, ObjectId uid) {
		this(name);
		setUserId(uid);
	}
	public Tag(BasicDBObject el) {
		setName(el.getString("name"));
	}
	public Tag(BasicDBObject el, ObjectId uid) {
		this(el);
		setUserId(uid);
	}
	public String getName() {
		return name;
	}
	@XmlTransient
	public ObjectId getUserId() {
		return user_id;
	}
	@XmlElement(name = "balance")
	public double getBalance() {
		double sum = 0;
		try {
			/*
			DBCollection tran = TransactionDAO.singleton().getCollection();
			
			BasicDBObject query = new BasicDBObject();
			query.put("user_id", getUserId());
			
			BasicDBObject initial = new BasicDBObject();
			initial.put("balance", 0);
			
			BasicDBList result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { p.balance += o.total; }");
			if (result.size() > 0) {
				DBObject r = (DBObject) result.get(0);
				double s = (double) r.get("balance");
				DecimalFormat twoDForm = new DecimalFormat("#.##");
				sum = Double.valueOf(twoDForm.format(s));
			}
			*/
		} catch (Exception e) {
			
		}
		
		return sum;
	}
	
	@Deprecated
	public static BSONObject getStats(User user, String tag, Calendar start_date, int interval, int period) {
	//	{initial: {balance: {}}, reduce: function (o, p) { var date = new Date(o.timestamp * 1000); date = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate(); p[date] = p[date] || 0; p[date] += o.total;}}
		BSONObject stats = new BasicBSONObject();
		Calendar tmp = (Calendar) start_date.clone();
		for (int i = 1; i <= period; i++) {
			String date  = tmp.get(Calendar.YEAR) + "-" + (tmp.get(Calendar.MONTH) + 1) + "-" + tmp.get(Calendar.DAY_OF_MONTH);
			stats.put(date, 0);
			tmp.add(interval, 1);
		}
		
		try {
			
			//old: function (o, p) { var key = Math.ceil((o.timestamp - " + startTime + ") / 86400); p.balance[key] = p.balance[key] || 0; p.balance[key] += o.total;}
			//new v1: db.transactions.group({initial: {balance: {}}, reduce: function (o, p) { var d = new Date(o.timestamp * 1000), key = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate(); p.balance[key] = p.balance[key] || 0; p.balance[key] += o.total; }});
			//new v2: db.transactions.group({initial: {balance: 0}, keyf: function (o) {var d = new Date(o.timestamp * 1000); return {"date": d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() };}, reduce: function (o, p) { p.balance += o.total; }});
			DBCollection tran = TransactionDAO.singleton().getCollection();
			
			BasicDBObject query = new BasicDBObject();
			query.put("user_id", user.getId());
			query.put("tags.name", tag);
			query.put("timestamp", new BasicDBObject("$gte", (int) (start_date.getTimeInMillis() / 1000)));
			
			BasicDBObject initial = new BasicDBObject();
			initial.put("balance", 0);
			
			String reduce = "function (o, p) { p.balance += o.total; }";
			String keyf = "function (o) {var d = new Date(o.timestamp * 1000); return {\"date\": d.getFullYear() + \"-\" + (d.getMonth() + 1) + \"-\" + d.getDate() };}";
			BasicDBObject args  = new BasicDBObject();
			
			args.put("initial", initial);
			args.put("cond", query);
			args.put("$reduce", reduce);
			args.put("$keyf", keyf);
			
			BasicDBList result = (BasicDBList) tran.group(args);
			
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			for (int i = 0; i < result.size(); i++) {
				BasicBSONObject r = (BasicBSONObject) result.get(i);
				Double dd = r.getDouble("balance");
				stats.put((String) r.get("date"), Double.valueOf(twoDForm.format(dd)));
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		return stats;
	}
	
	public void setName(String value) {
		if (validateName(value)) {
			name = value.toLowerCase();
		}
	}
	public void setUserId(ObjectId value) {
		user_id = value;
	}
	
	public static boolean validateName(String value) {
		return value != null
			&& value.length() >= 2 && value.length() <= 24
			&& value.matches(NAME_REGEX);
	}
}