package ro.weednet.wmg.model;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.bson.types.ObjectId;

import ro.weednet.wmg.dao.CurrencyDAO;
import ro.weednet.wmg.dao.TransactionDAO;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.NotSaved;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

@XmlRootElement(name = "Budget")
@Entity("budgets")
public class Budget {
	public static final short TYPE_DEPOSIT = Transaction.TYPE_DEPOSIT;
	public static final short TYPE_WITHDRAWAL = Transaction.TYPE_WITHDRAWAL;
	public static final short TYPE_DAILY = 1<<2;
	public static final short TYPE_WEEKLY = 1<<3;
	public static final short TYPE_MONTHLY = 1<<4;
	public static final short TYPE_QUARTERLY = 1<<5;
	public static final short TYPE_HALF_YEARLY = 1<<6;
	public static final short TYPE_YEARLY = 1<<7;
	
	@Id
	private ObjectId _id;
	private ObjectId user_id;
	private String name;
	private Tag tag;
	private double value;
	private String currency;
	private int length;
	private boolean is_hidden;
	private short type;
	@XmlElement(name = "level")
	@NotSaved
	private double level;
	@XmlElement(name = "last_level")
	@NotSaved
	private double last_level;
	@XmlElement(name = "level_avg")
	@NotSaved
	private double level_avg;
	
	public Budget() {
		this(null, null, null, 0, null, 0, (short) (TYPE_DEPOSIT & TYPE_WITHDRAWAL));
	}
	public Budget(ObjectId uid, String name, Tag tag, double value, String currency, int length, short type) {
		setUserId(uid);
		setName(name);
		setTag(tag);
		setValue(value);
		setCurrency(currency);
		setLength(length);
		setType(type);
	}
	
	public ObjectId getId() {
		return _id;
	}
	@XmlElement(name = "id")
	public String getIdSring() {
		return _id.toString();
	}
	public String getName() {
		return name;
	}
	public Tag getTag() {
		return tag;
	}
	public double getValue() {
		return value;
	}
	public String getCurrency() {
		return currency;
	}
	public int getLength() {
		return length;
	}
	public int getStartTimestamp()
	{
		Calendar date = Calendar.getInstance();
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		
		//TODO: use length ..
		if ((type & TYPE_DAILY) > 0) {
			//nothing ..
		} else if ((type & TYPE_WEEKLY) > 0) {
			date.set(Calendar.DAY_OF_WEEK, date.getFirstDayOfWeek());
		} else if ((type & TYPE_MONTHLY) > 0) {
			date.set(Calendar.DAY_OF_MONTH, 1);
		} else if ((type & TYPE_QUARTERLY) > 0) {
			int quarter = date.get(Calendar.MONTH) / 3 + 1;
			date.set(Calendar.DAY_OF_MONTH, 1);
			date.set(Calendar.MONTH, (quarter - 1) * 3);
		} else if ((type & TYPE_HALF_YEARLY) > 0) {
			int half = date.get(Calendar.MONTH) / 6 + 1;
			date.set(Calendar.DAY_OF_MONTH, 1);
			date.set(Calendar.MONTH, (half - 1) * 6);
		} else if ((type & TYPE_YEARLY) > 0) {
			date.set(Calendar.DAY_OF_MONTH, 1);
			date.set(Calendar.MONTH, Calendar.JANUARY);
		}
		
		return (int) (date.getTimeInMillis() / 1000);
	}
	public short getType() {
		return type;
	}
	@XmlTransient
	public ObjectId getUserId() {
		return user_id;
	}
	@XmlElement(name = "is_hidden")
	public boolean getIsHidden() {
		return is_hidden;
	}
	
	public boolean acceptsDeposits() {
		return (type & TYPE_DEPOSIT) > 0;
	}
	public boolean acceptsWithdrawals() {
		return (type & TYPE_WITHDRAWAL) > 0;
	}
	
	public int shiftTimestamp(int timestamp, int amount) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis((long) timestamp * 1000); 
		
		if ((type & TYPE_DAILY) > 0) {
			date.add(Calendar.DAY_OF_MONTH, amount);
		} else if ((type & TYPE_WEEKLY) > 0) {
			date.add(Calendar.WEEK_OF_YEAR, amount);
		} else if ((type & TYPE_MONTHLY) > 0) {
			date.add(Calendar.MONTH, amount);
		} else if ((type & TYPE_QUARTERLY) > 0) {
			date.add(Calendar.MONTH, amount * 3);
		} else if ((type & TYPE_HALF_YEARLY) > 0) {
			date.add(Calendar.MONTH, amount * 6);
		} else if ((type & TYPE_YEARLY) > 0) {
			date.add(Calendar.YEAR, amount);
		}
		
		return (int) (date.getTimeInMillis() / 1000);
	}
	public void processData() {
		level = 0;
		last_level = 0;
		level_avg = 0;
		
		try {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			DBCollection tran = TransactionDAO.singleton().getCollection();
			
			BasicDBObject initial = new BasicDBObject();
			BasicDBObject query = new BasicDBObject();
			query.put("user_id", getUserId());
			query.put("type", (getType() & (TYPE_DEPOSIT | TYPE_WITHDRAWAL)));
			query.put("tags.name", tag.getName());
			
			//current level
			int start = getStartTimestamp();
			BasicDBObject timestampCond = new BasicDBObject();
			timestampCond.put("$gte", start);
			timestampCond.put("$lt", shiftTimestamp(start, length));
			query.put("timestamp", timestampCond);
			
			BasicDBList result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { if (o.currency in p) p[o.currency] += o.value; else p[o.currency] = o.value; }");
			
			if (result.size() > 0) {
				level = sumCurrencies((DBObject) result.get(0), currency);
			}
			
			//last level
			timestampCond = new BasicDBObject();
			timestampCond.put("$gte", shiftTimestamp(start, -length));
			timestampCond.put("$lt", start);
			query.put("timestamp", timestampCond);
			
			result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { if (o.currency in p) p[o.currency] += o.value; else p[o.currency] = o.value; }");
			
			if (result.size() > 0) {
				last_level = sumCurrencies((DBObject) result.get(0), currency);
			}
			
			//level prev avg
			int avgLength = 6;
			timestampCond = new BasicDBObject();
			timestampCond.put("$gte", shiftTimestamp(start, -length * avgLength));
			timestampCond.put("$lt", start);
			query.put("timestamp", timestampCond);
			
			result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { if (o.currency in p) p[o.currency] += o.value; else p[o.currency] = o.value; }");
			
			if (result.size() > 0) {
				try {
					level_avg = Double.valueOf(twoDForm.format(sumCurrencies((DBObject) result.get(0), currency) / avgLength));
				} catch (Exception e) { }
			}
		} catch (Exception e) {
			System.out.println("3.2.4: " + e.toString());
		}
	}
	
	public void setName(String value) {
		if (validateName(value)) {
			name = value;
		}
	}
	public void setTag(Tag value) {
		tag = value;
	}
	public void setValue(double value) {
		if (validateValue(value)) {
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			this.value = Double.valueOf(twoDForm.format(value));
		}
	}
	public void setCurrency(String value) {
		if (validateCurrency(value)) {
			currency = value;
		}
	}
	public void setLength(int value) {
		if (validateLength(value)) {
			length = value;
		}
	}
	public void setType(short value) {
		if (validateType(value)) {
			type = value;
		}
	}
	public void setUserId(String value) {
		user_id = new ObjectId(value);
	}
	public void setUserId(ObjectId value) {
		user_id = value;
	}
	public void setHidden(boolean value) {
		is_hidden = value;
	}
	
	public static boolean validateName(String value) {
		return value != null
			&& value.length() >= 2 && value.length() <= 24;
	}
	public static boolean validateValue(double value) {
		return value > 0 && value < 1000000000d;
	}
	public static boolean validateLength(int value) {
		return value > 0 && value < 365;
	}
	public static boolean validateCurrency(String value) {
		try {
			if (CurrencyDAO.singleton().findOne(value) != null) {
				return true;
			}
		} catch (Exception e) { }
		
		return false;
	}
	public static boolean validateType(short value) {
		return ((value & TYPE_DEPOSIT) > 0 || (value & TYPE_WITHDRAWAL) > 0)
		 && (
		    (value & TYPE_DAILY) > 0
		 || (value & TYPE_WEEKLY) > 0
		 || (value & TYPE_MONTHLY) > 0
		 || (value & TYPE_QUARTERLY) > 0
		 || (value & TYPE_HALF_YEARLY) > 0
		 || (value & TYPE_YEARLY) > 0
		);
	}
	
	private static double sumCurrencies(DBObject r, String defaultCurrency) {
		double sum = 0;
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		
		Set<String> keys = r.keySet();
		Iterator<String> it = keys.iterator();
		
		while (it.hasNext()) {
			String c = it.next();
			try {
				double b = Double.valueOf(twoDForm.format((double) r.get(c)));
				if (c == defaultCurrency) {
					sum += b;
				} else {
					sum += b * Currency.getRate(c, defaultCurrency);
				}
			} catch (Exception e) { }
		}
		return sum;
	}
}