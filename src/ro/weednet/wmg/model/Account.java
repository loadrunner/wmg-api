package ro.weednet.wmg.model;

import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.bson.types.ObjectId;

import ro.weednet.common.Pair;
import ro.weednet.wmg.dao.TransactionDAO;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.NotSaved;
import com.google.code.morphia.query.Query;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;

@XmlRootElement(name = "Account")
@Entity("accounts")
public class Account {
	@Id
	private ObjectId _id;
	private ObjectId user_id;
	private String name;
	private String currency;
	private boolean is_hidden;
	private int created_at;
	private int modified_at;
	@XmlElement(name = "balance")
	@NotSaved
	private double balance;
	
	public Account() {
		this((ObjectId) null, null, null);
	}
	public Account(User user, String name, String currency) {
		this(user.getId(), name, currency);
	}
	public Account(ObjectId user_id, String name, String currency) {
		setUserId(user_id);
		setName(name);
		setCurrency(currency);
		created_at = (int) (System.currentTimeMillis() / 1000);
	}
	
	public ObjectId getId() {
		return _id;
	}
	@XmlElement(name = "id")
	public String getIdString() {
		return _id.toString();
	}
	@XmlTransient
	public ObjectId getUserId() {
		return user_id;
	}
	@XmlElement(name = "user_id")
	public String getUserIdSring() {
		return user_id.toString();
	}
	public String getName() {
		return name;
	}
	public String getCurrency() {
		return currency;
	}
	@XmlElement(name = "is_hidden")
	public boolean getIsHidden() {
		return is_hidden;
	}
	public int getCreatedAt() {
		return created_at;
	}
	public int getModifiedAt() {
		return modified_at;
	}
	
	public void calculateBalance() {
		double sum = 0;
		try {
			DBCollection tran = TransactionDAO.singleton().getCollection();
			
			BasicDBObject query = new BasicDBObject();
			query.put("account_id", getId());
			
			BasicDBObject initial = new BasicDBObject();
			initial.put("balance", 0);
			
			BasicDBList result = (BasicDBList) tran.group(null, query, initial, "function (o, p) { p.balance += o.total; }");
			if (result.size() > 0) {
				DBObject r = (DBObject) result.get(0);
				double s = (double) r.get("balance");
				DecimalFormat twoDForm = new DecimalFormat("#.##");
				sum = Double.valueOf(twoDForm.format(s));
			}
		} catch (Exception e) {
			
		}
		
		balance = sum;
	}
	
	public ArrayList<Transaction> getTransactions() {
		return getTransactions(null, 0);
	}
	public ArrayList<Transaction> getTransactions(String keywords, int offset) {
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		
		try {
			TransactionDAO dao = TransactionDAO.singleton();
			Query<Transaction> query = dao.createQuery();
			query.filter("account_id", getId());
			if (keywords != null && keywords.length() > 0) {
				Pair<String, ArrayList<Tag>> r = Transaction.parseDescription(keywords);
				if (r.first != null && r.first.length() > 0) {
					String[] words = r.first.split("([ ]+)");
					String regex = "";
					for (int i = 0; i < words.length; i++) {
						if (words[i].length() > 0) {
							if (regex.length() > 0) {
								regex += "(.*)?";
							}
							regex += Pattern.quote(words[i]);
						}
					}
					
					if (regex.length() > 0) {
						query.filter("description", Pattern.compile(regex));
					}
				}
				
				if (r.second != null && r.second.size() > 0) {
					for (int i = 0; i < r.second.size(); i++) {
						query.filter("tags.name", r.second.get(i).getName());
					}
				}
			}
			query.order("-timestamp, -_id");
			if (offset > 0) {
				query.offset(offset).limit(25);
			} else {
				query.limit(50);
			}
			Iterable<Transaction> i = dao.find(query).fetch();
			
			for(Transaction transaction: i) {
				transactions.add(transaction);
			}
		} catch (UnknownHostException | MongoException e) {
			
		}
		
		return transactions;
	}
	
	public void setName(String value) {
		if (validateName(value)) {
			name = value;
		}
	}
	public void setCurrency(String value) {
		currency = value;
	}
	public void setUserId(String value) {
		user_id = new ObjectId(value);
	}
	public void setUserId(ObjectId value) {
		user_id = value;
	}
	public void setHidden(boolean value) {
		is_hidden = value;
	}
	
	public static boolean validateName(String value) {
		return value != null
			&& value.length() >= 2 && value.length() <= 32
			&& value.matches("([a-zA-Z]+)([a-zA-Z0-9-_]*)([a-zA-Z]+)");
	}
}